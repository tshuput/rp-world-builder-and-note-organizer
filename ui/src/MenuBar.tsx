import React from "react";
import { Link, useLocation } from "react-router-dom";
import { Text, View, Flex } from "@adobe/react-spectrum";

const menuItems = [
  {
    path: "/",
    label: "Home",
  },
  { path: "/home-2", label: "Home 2" },
];

const MenuBar = () => {
  const location = useLocation();

  return (
    <View position="sticky" top={0} backgroundColor={"gray-200"}>
      <Flex direction="row" height="size-400" gap="size-100">
        {menuItems.map((item) => (
          <View
            key={item.label}
            backgroundColor={
              location.pathname === item.path ? "gray-400" : undefined
            }
            padding="size-100"
            paddingTop="size-50"
          >
            <Link
              to={item.path}
              style={{ textDecoration: "none", color: "inherit" }}
            >
              <Text>{item.label}</Text>
            </Link>
          </View>
        ))}
      </Flex>
    </View>
  );
};

export default MenuBar;
