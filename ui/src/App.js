import { Provider, defaultTheme } from "@adobe/react-spectrum";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { routes } from "./routes";
import MenuBar from "./MenuBar";

function App() {
  return (
    <Provider theme={defaultTheme}>
      <Router>
        <MenuBar />
        <Switch>
          {routes.map((route) => (
            <Route path={route.path} component={route.Component} />
          ))}
        </Switch>
      </Router>
    </Provider>
  );
}

export default App;
