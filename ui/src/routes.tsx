import Home from "./Home";
import Home2 from "./Home2";

export const routes = [
  {
    path: "/home-2",
    Component: Home2,
  },
  {
    path: "/",
    Component: Home,
  },
];
